/**
 * itsc.ajax.js
 * @file AJAX helper methods
 *
 * @version 2.0.0
 * @author  Eric Dieckman
 * @updated 2018-05-23
 * @link    https://git.aae.wisc.edu/open-source/itsc.ajax.git
 *
 * AJAX methods
 * @module Ajax
 * 
 * @requires jquery
 * @requires module:CommonUI
 */

var Ajax = (function () {

    /* =================== private methods ================= */

    /**
     * Displays an error dialog with a StackTrace (if available)
     * @param {Object} error_response - response object from server - already parsed
     * @private
     */
    var _on_failed = function (error_response) {
        var html = '<p>An error occurred during AJAX call:</p>';

        if (error_response.InnerException) {
            html += '<p><strong>Inner Message:</strong> ' + error_response.InnerException.Message + '&nbsp;&nbsp;&nbsp;<strong>Inner ExceptionType:</strong> ' + error_response.InnerException.ExceptionType + '</p>'
                + '<p><strong>Inner ExceptionMessage:</strong> <em>' + error_response.InnerException.ExceptionMessage + '</em></p>'
                + '<p><strong>Inner StackTrace:</strong><br /><small class="collapse" id="inner_stacktrace">' + error_response.InnerException.StackTrace + '</small>'
                + '<a data-toggle="collapse" data-target="#inner_stacktrace" class="btn"> View Stacktrace &raquo;</a></p> ';
        }

        html += '<p><strong>Message:</strong> ' + error_response.Message + '&nbsp;&nbsp;&nbsp;<strong>ExceptionType:</strong> ' + error_response.ExceptionType + '</p>'
            + '<p><strong>ExceptionMessage:</strong> <em>' + error_response.ExceptionMessage + '</em></p>'
            + '<p><strong>StackTrace:</strong><br /><small class="collapse" id="outer_stacktrace">' + error_response.StackTrace + '</small>'
            + '<a data-toggle="collapse" data-target="#outer_stacktrace" class="btn"> View Stacktrace &raquo;</a></p> ';


        CommonUI.error_dialog('AJAX Error', html);
        CommonUI.unblockUI();
    };

    /* =================== public methods ================== */

    /**
     * Executes an AJAX request
     * @method
     * @param {string} address - URL to send the ajax request to
     * @param {string} type - HTTP method (GET/POST/PUT)
     * @param {Object} data - data to sent with the request
     * @param {module:Ajax~on_success} on_success - A function to be called if the request succeeds
     * @param {module:Ajax~on_fail} [on_fail] - A function to be called if the request fails
     * @returns {Promise}
     * @static
     */
    var request = function (address, type, data, on_success, on_fail) {

        var ajax_options = {
            type: type
            , crossDomain: true
            , url: address
            , contentType: "application/json; charset=utf-8"
            , dataType: "json"
        };


        if (!jQuery.isEmptyObject(data) && type != 'GET') {
            //var pairs = [];
            //for (var i in data) {
            //    pairs.push(i + ':' + JSON.stringify(data[i]));
            //}
            ajax_options.data = JSON.stringify(data);
        } else if (!jQuery.isEmptyObject(data) && type == 'GET') {
            tail = [];
            $.each(data, function (key, value) {
                tail.push(key + "=" + encodeURIComponent(value));
            })
            ajax_options.url += '?' + tail.join("&");
        }


        // Perform the post operation
        var promiseObj = $.ajax(ajax_options);

        if ($.isFunction(on_success)) {
            promiseObj.done(on_success);
        }
        promiseObj.fail(function (jqXHR, textStatus, errorThrown) {
            // The .NET error and stacktrace is hidden
            // inside the XMLHttpRequest response
            var error_response = $.parseJSON(jqXHR.responseText);
            if ($.isFunction(on_fail)) {
                on_fail(error_response);
            }
            else {
                _on_fail(error_response);
            }
                
        });

        return promiseObj;
    };

    /**
     * Executes an AJAX GET request
     * @method
     * @param {string} address - URL to send the ajax request to
     * @param {module:Ajax~on_success} on_success - A function to be called if the request succeeds
     * @param {module:Ajax~on_fail} [on_fail] - A function to be called if the request fails
     * @returns {Promise}
     * @static
     */
    var get = function(address, on_success, on_fail){
        return request(address, 'GET', {}, on_success, on_fail);
    };

    /* =============== export public methods =============== */

    return {
        /**
         * Callback for a success ajax request
         * @callback on_success
         * @param {any} data - data returned from the AJAX request
         * @param {string} textStatus - string categorizing the status of the request
         * @param {Object} jqXHR - superset of the browser's native XMLHttpRequest object
         */

        /**
         * Callback for a failed ajax request
         * @callback on_fail
         * @param {Object} error_response - response object from server - already parsed
         */

        /**
         * Executes an AJAX request
         * @method
         * @param {string} address - URL to send the ajax request to
         * @param {string} type - HTTP method (GET/POST/PUT)
         * @param {Object} data - data to sent with the request
         * @param {module:Ajax~on_success} on_success - A function to be called if the request succeeds
         * @param {module:Ajax~on_fail} [on_fail] - A function to be called if the request fails
         * @returns {Promise}
         * @static
         */
        request: request,

        /**
         * Executes an AJAX GET request
         * @method
         * @param {string} address - URL to send the ajax request to
         * @param {module:Ajax~on_success} on_success - A function to be called if the request succeeds
         * @param {module:Ajax~on_fail} [on_fail] - A function to be called if the request fails
         * @returns {Promise}
         * @static
         */
        get: get
    };

})();

